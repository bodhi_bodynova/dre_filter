<?php
/**
 * Metadata version
*/
$sMetadataVersion = '2.0';
/**
 * Module information
 *	Dieses Modul stellt Filter Möglichkeiten in der Listenansicht zur verfügung.
 */
$aModule = array(
    'id'           => 'dre_filter',
    'title'        => '<img src="../modules/bender/dre_filter/out/img/favicon.ico" title="Bender Clear Temp Modul">ody Filter',
    'description'  => array(
        'de' => 'Modul Attribut Filter Oxid Shop.'
    ),
    'thumbnail'     => 'out/img/logo_bodynova.png',
    'version'       => '2.0.0',
    'author'        => 'Andre Bender',
    'url'           => 'https://bodynova.de',
    'email'         => 'a.bender@bodynova.de',
    'extend'	    => [
        \OxidEsales\Eshop\Application\Model\AttributeList::class => \Bender\dre_Filter\Model\AttributeList::class,
	],
    'settings'    => [
    ],
    'blocks'      => [
    ],
);
